# README #

MQTT API is Simple code Java to connect, publish, subscribe to broker of IoT with Paho library. 

## How to use it ##
* Run Sender.java and Receiver.java (Shift + F6)
* Connect to Host "test.mosquitto.org" / "iot.eclipse.org"
* Do publish with a topic from Sender.java and Subscribe a topic that was published. 

### Fork and let's discuss about this API ###